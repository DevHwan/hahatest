#include "stdafx.h"
#include "MyDLL.h"

Hello::Hello()
    : m_ival(0)
{

}

int Hello::GetVal() const
{
    return m_ival;
}

void Hello::Update()
{
    m_ival++;
}
