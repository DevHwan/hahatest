#pragma once

#if defined(MYDLL_EXPORTS)
#   define MYDLL_API __declspec(dllexport)
#else
#   define MYDLL_API __declspec(dllimport)
#endif

class MYDLL_API Hello
{
public:
    Hello();
    virtual ~Hello() = default;

    int GetVal() const;
    virtual void Update();
private:
    int m_ival;
};
