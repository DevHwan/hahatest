#include "pch.h"
#include "../MyDLL/MyDLL.h"

TEST(TestCaseName, TestName) {
    Hello hello;
    EXPECT_EQ(hello.GetVal(), 0);
    hello.Update();
    EXPECT_EQ(hello.GetVal(), 1);
}